package com.example.CalculatingPathWithFreeMarker.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PathFinderServiceImplTest {

    @Test
    void getPathFromUserFile() {

        List<String> listAllLines = List.of(
                "$GPGGA,092547.20,100.000,N,3.3,E,1,34,0.5,100.3851,M,-1.6215,M,02,999*4F",
                "$GNVTG,28.858,T,17.799,M,0.0111,N,0.10279,K,D*3E",
                "$GPGGA,092547.30,500.000,N,3.3,E,1,34,0.5,100.3837,M,-1.6215,M,02,999*44",
                "$GNVTG,191.772,T,180.713,M,0.222,N,0.02505,K,D*39",
                "$GPGGA,092547.40,700.000,N,3.3,E,1,34,0.5,100.3834,M,-1.6215,M,02,999*4F",
                "$GNVTG,209.494,T,198.434,M,0.444,N,0.04084,K,D*34",
                "$GPGGA,092547.40,800.000,N,3.3,E,1,34,0.5,100.3834,M,-1.6215,M,02,999*4F",
                "$GNVTG,209.494,T,198.434,M,0.444,N,0.04084,K,D*34"
                );

        double minSpeed = 0.02;

        boolean isFirstCoords = true;

        double lat1 = 0.0;
        double lon1 = 0.0;
        double lat2;
        double lon2;
        double result = 0.0;

        for (int i = 0; i < listAllLines.size(); i++) {

            // поиск первых координат
            if (isFirstCoords && listAllLines.get(i).length() > 50 && listAllLines.get(i).startsWith("$GPGGA")) {

                lat1 = Double.parseDouble(listAllLines.get(i).split(",")[2]);
                lon1 = Double.parseDouble(listAllLines.get(i).split(",")[4]);
                isFirstCoords = false;
                continue;
            }

            if (listAllLines.get(i).length() > 45 && listAllLines.get(i).startsWith("$GNVTG")) {

                // если скорость меньше заданного значения
                if (Double.parseDouble(listAllLines.get(i).split(",")[5]) < minSpeed) {

                    ++i; // следующие координаты пропускаются
                    isFirstCoords = true; //
                    continue;
                }
            }

            // поиск последующих координат, вычисление пройденного пути, добавление в результирующий путь
            if (listAllLines.get(i).length() > 50 && listAllLines.get(i).startsWith("$GPGGA")) {

                lat2 = Double.parseDouble(listAllLines.get(i).split(",")[2]);
                lon2 = Double.parseDouble(listAllLines.get(i).split(",")[4]);
                result = result + DistanceCoords.getDistanceFromDegreesWithMinutes(lat1, lon1, lat2, lon2);
                lat1 = lat2;
                lon1 = lon2;
            }
        }

        assertEquals (111.0, Math.round(result));

    }

    @Test
    void getPathFromCoords() {

        Double result0 = DistanceCoords.getDistanceFromDegrees(1.00, 3.3, 1.00, 3.3);
        assertEquals (0.0, result0);

        Double result = DistanceCoords.getDistanceFromDegrees(1.00, 3.3, 2.00, 3.3);
        assertEquals (111.0, Math.round(result));
    }
}