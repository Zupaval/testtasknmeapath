package com.example.CalculatingPathWithFreeMarker.service;

public class DistanceCoords {

    // средний радиус Земли
    private static final Double RADIUS_OF_EARTH = 6371.0;

    /**
     * Преобразование градусов в радианы для формулы гаверсинусов
     *
     * @param degree градусы с десятичной дробью в формате ГГГГ.ГГ
     * @return радианы
     */
    private static Double degreeToRadian(Double degree) {

        return degree * (Math.PI / 180);
    }


    /**
     * Преобразование градусов с минутами в градусы
     *
     * @param degreeWithMinutes градусы и минуты с десятичной дробью в формате ГГММ.ММ
     * @return градусы с десятичной дробью в формате ГГГГ.ГГ
     */
    private static Double degreeWithMinutesToDegree(Double degreeWithMinutes) {

        return (Math.floor(degreeWithMinutes / 100) * 60 + degreeWithMinutes % 100) / 60;
    }

    /**
     * Вычисление расстояния в радианах по формуле гаверсинусов
     *
     * @param lat1 широта первой точки
     * @param lon1 долгота первой точки
     * @param lat2 широта второй точки
     * @param lon2 долгота второй точки
     * @return угловое расстояние в радианах
     */
    private static Double getHaversine(Double lat1, Double lon1, Double lat2, Double lon2) {

        return 2 * Math.asin(Math.sqrt(Math.pow(Math.sin((lat2 - lat1) / 2), 2) +
                Math.cos(lat1) * Math.cos(lat2) *
                        Math.pow(Math.sin((lon2 - lon1) / 2), 2)));
    }

    /**
     * Вычисление расстояния в км (координаты - градусы и минуты с десятичной дробью в формате ГГММ.ММ)
     *
     * @param lat1 широта первой точки
     * @param lon1 долгота первой точки
     * @param lat2 широта второй точки
     * @param lon2 долгота второй точки
     * @return расстояние в км
     */
    public static Double getDistanceFromDegreesWithMinutes(Double lat1, Double lon1, Double lat2, Double lon2) {

        lat1 = degreeToRadian(degreeWithMinutesToDegree(lat1));
        lon1 = degreeToRadian(degreeWithMinutesToDegree(lon1));
        lat2 = degreeToRadian(degreeWithMinutesToDegree(lat2));
        lon2 = degreeToRadian(degreeWithMinutesToDegree(lon2));

        Double distanceInRadian = getHaversine(lat1, lon1, lat2, lon2);

        return RADIUS_OF_EARTH * distanceInRadian;
    }

    /**
     * Вычисление расстояния в км (координаты - градусы с десятичной дробью в формате ГГГГ.ГГ)
     *
     * @param lat1 широта первой точки
     * @param lon1 долгота первой точки
     * @param lat2 широта второй точки
     * @param lon2 долгота второй точки
     * @return расстояние в км
     */
    public static Double getDistanceFromDegrees(Double lat1, Double lon1, Double lat2, Double lon2) {

        lat1 = degreeToRadian(lat1);
        lon1 = degreeToRadian(lon1);
        lat2 = degreeToRadian(lat2);
        lon2 = degreeToRadian(lon2);

        Double distanceInRadian = getHaversine(lat1, lon1, lat2, lon2);

        return RADIUS_OF_EARTH * distanceInRadian;
    }

}
