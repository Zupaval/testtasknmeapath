package com.example.CalculatingPathWithFreeMarker.controller;


import com.example.CalculatingPathWithFreeMarker.service.PathFinderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@Controller
@RequestMapping("/pathFinderFromUserFile")
public class PathFinderFromUserFileController {

    private final PathFinderService pathFinderService;

    @Autowired
    public PathFinderFromUserFileController(PathFinderService pathFinderService) {
        this.pathFinderService = pathFinderService;
    }

    @GetMapping("/")
    public String pathFinder() {
        return "pathFinderFromUserFile";
    }

    @PostMapping("/setPath")
    public String uploadSingleFile(@RequestParam("file") MultipartFile file, Double minSpeed, Model model) {

        model.addAttribute("calculatedPath", pathFinderService.getPathFromUserFile(file, minSpeed));
        return "pathFinderFromUserFileResult";
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @GetMapping("/error")
    public String onRuntimeException(Exception e, Model model) {

        model.addAttribute("message", e.getMessage());
        return "error";
    }


}
