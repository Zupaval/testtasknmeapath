package com.example.CalculatingPathWithFreeMarker.controller;

import com.example.CalculatingPathWithFreeMarker.service.PathFinderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/pathFinderFromCoords")
public class PathFinderCoordsController {

    private final PathFinderService pathFinderService;

    @Autowired
    public PathFinderCoordsController(PathFinderService pathFinderService) {
        this.pathFinderService = pathFinderService;
    }

    @GetMapping("/")
    public String pathFinder() {

        return "pathFinderFromCoords";
    }

    @PostMapping("/setCoords")
    public String setCoords(Double lat1, Double lon1, Double lat2, Double lon2, Model model) {

        model.addAttribute("calculatedPath", pathFinderService.getPathFromCoords(lat1, lon1, lat2, lon2));
        return "pathFinderFromCoordsResult";
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @GetMapping("/error")
    public String onRuntimeException(Exception e, Model model) {

        model.addAttribute("message", e.getMessage());
        return "error";
    }
}