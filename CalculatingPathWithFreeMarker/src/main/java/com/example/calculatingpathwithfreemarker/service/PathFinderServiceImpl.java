package com.example.CalculatingPathWithFreeMarker.service;


import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

@Service
public class PathFinderServiceImpl implements PathFinderService {

    private static final Logger log = Logger.getLogger(PathFinderServiceImpl.class);

    /**
     * Расчет пути по географическим координатам из лог-файла формата NMEA
     *
     * @param file местоположение лог-файла с координатами
     * @param minSpeed минимальная скорость, при которой изменение координат не учитывается (узлы)
     * @return строка с результатом вычисления пройденного пути (км с округлением до 3-го знака после запятой)
     */
    public String getPathFromUserFile(MultipartFile file, Double minSpeed) {


        // создание пути для нового файла на сервере в папке nmeaLogFiles с именем загруженного файла
        Path newPath = Paths.get("CalculatingPathWithFreeMarker/src/main/resources/static/nmeaLogFiles",
                Objects.requireNonNull(file.getOriginalFilename()));

        // создание нового файла (перезапись) и копирование в него содержимого из загруженного файла
        try {
            Files.deleteIfExists(newPath);
            Files.createFile(newPath);
            file.transferTo(newPath);
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }

        List<String> listAllLines;

        boolean isFirstCoords = true;

        Path path = Paths.get(newPath.toUri());

        // построчное чтение нового файла
        try {
            listAllLines = Files.readAllLines(path);
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }

        double lat1 = 0.0;
        double lon1 = 0.0;
        double lat2;
        double lon2;
        double result = 0.0;

        // поиск координат и скорости
        for (int i = 0; i < listAllLines.size(); i++) {

            // поиск первых координат
            if (isFirstCoords && listAllLines.get(i).length() > 50 && listAllLines.get(i).startsWith("$GPGGA")) {

                lat1 = Double.parseDouble(listAllLines.get(i).split(",")[2]);
                lon1 = Double.parseDouble(listAllLines.get(i).split(",")[4]);
                isFirstCoords = false;
                continue;
            }

            if (listAllLines.get(i).length() > 45 && listAllLines.get(i).startsWith("$GNVTG")) {

                // если скорость меньше заданного значения
                if (Double.parseDouble(listAllLines.get(i).split(",")[5]) < minSpeed) {

                    ++i; // следующие координаты пропускаются
                    isFirstCoords = true; //
                    continue;
                }
            }

            // поиск последующих координат, вычисление пройденного пути, добавление в результирующий путь
            if (listAllLines.get(i).length() > 50 && listAllLines.get(i).startsWith("$GPGGA")) {

                lat2 = Double.parseDouble(listAllLines.get(i).split(",")[2]);
                lon2 = Double.parseDouble(listAllLines.get(i).split(",")[4]);
                result = result + DistanceCoords.getDistanceFromDegreesWithMinutes(lat1, lon1, lat2, lon2);
                lat1 = lat2;
                lon1 = lon2;
            }
        }
        log.info("Выполнен метод getPathFromUserFile c файлом: " + newPath +
                ",  мин скоростью: " + minSpeed + " и результатом: " + result);

        return String.format("%.3f", result) + " км";

    }

    /**
     * Расчет пути по географическим координатам, указанным отдельно
     *
     * @param lat1 широта первой точки
     * @param lon1 долгота первой точки
     * @param lat2 широта второй точки
     * @param lon2 долгота второй точки
     * @return строка с результатом вычисления пройденного пути (км с округлением до 3-го знака после запятой)
     */
    @Override
    public String getPathFromCoords(Double lat1, Double lon1, Double lat2, Double lon2) {

        Double result = DistanceCoords.getDistanceFromDegrees(lat1, lon1, lat2, lon2);
        log.info("Выполнен метод getPathFromCoords c начальной широтой: " + lat1 +
                ", начальной долготой: " + lon1 + ", конечной широтой: " + lat2 +
                ", конечной долготой" + lon2 + " и результатом: " + result);

        return String.format("%.3f", result) + " км";
    }
}
