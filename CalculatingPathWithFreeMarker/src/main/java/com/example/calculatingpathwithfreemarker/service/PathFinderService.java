package com.example.CalculatingPathWithFreeMarker.service;

import org.springframework.web.multipart.MultipartFile;

public interface PathFinderService {

    String getPathFromUserFile(MultipartFile file, Double minSpeed);

    String getPathFromCoords(Double lat1, Double lon1, Double lat2, Double lon2);
}
