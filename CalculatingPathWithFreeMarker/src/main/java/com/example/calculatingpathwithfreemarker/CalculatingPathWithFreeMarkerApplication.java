package com.example.CalculatingPathWithFreeMarker;
/*
Тестовое задание:

"Написать программу, которая берет из файла nmea.log координаты и считает
по ним пройденный путь.
Если скорость равна нулю, изменение координат в пройденном пути не
учитывать.

Дополнительное задание, для демонстрации навыков веба - сделать
простейшую веб-форму для решения этой же задачи. Выбор способов и
технологий - на ваше усмотрение."

Для расширения функционала добавил возможность загрузить файл nmea.log со стороны клиента,
также возможность задать минимальную скорость, при которой изменение координат
в пройденном пути не будет учитываться.

Для решения тестового задания использовался следующий стек технологий:
Java SE
Spring MVC
Springboot
FreeMarker
HTML
CSS
Log4J
JUnit
GitLab
 */


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@SpringBootApplication
public class CalculatingPathWithFreeMarkerApplication {

    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        // максимальный размер загружаемого файла 100MB
        multipartResolver.setMaxUploadSize(100*1024*1024);
        return multipartResolver;
    }

    public static void main(String[] args) {
        SpringApplication.run(CalculatingPathWithFreeMarkerApplication.class, args);
    }

}
